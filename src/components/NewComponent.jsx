import React from "react";

export default class NewComponents extends React.Component{
    
    render(){
        const {miau,gua,manageF}=this.props;
        return(
             <table>
                <thead>
                    <th>Name</th>
                    <th>Location</th>
                </thead>
                <tbody>
                    <td>{miau}</td>
                    <td>{gua}</td>
                    <td onClick={manageF}>click me</td>
                </tbody>
             </table>
            
        );
    }
}