import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import {
  Table,
  Button,
  Container,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  FormGroup
} from 'reactstrap'

import React from 'react';
// import ViewList from './components/ViewList';
// import UserForm from './components/UserForm';

const data =[
  {id:1, username:'Ray parra',email:'ray@parra.com'},
  {id:2, username:'Manuel Gomez',email:'manuelgom3z@gmail.com'},
  {id:3, username:'Nagato Uzumaki',email:'nakato123@gmail.com'},
  {id:4, username:'Gael breton',email:'gael123@hotmail.com'},
]

//components of lab 1
export default class APP extends React.Component{
       state={
        data:data
       }

      // handleCHange=(e)=>{
      //   let value=e.target.textContent;
      //   if(value==="add"){
      //     this.setState({path:'form'})
      //      e.target.textContent='list'
      //   }else if(value==="list"){
      //     this.setState({path:'list'})
      //     e.target.textContent='add'
      //   }
      // }

    //  changeState=(ruta)=>{
    //     this.setState({path:ruta})
    //   }


    // router=(ruta)=>{
    //     this.changeState(ruta)   
    // }
   
     render(){
      return(
        <div className="App">
          <Container>
             <Button color='outline-success'>New user</Button>
             <Table>
                <thead>
                  <th>ID</th>
                  <th>USERNAME</th>
                  <th>EMAIL</th>
                  <th>ACTION</th>
                </thead>
                <tbody>
                  {this.state.data.map((element)=>(
                      <tr>
                         <td>{element.id}</td>
                         <td>{element.username}</td>
                         <td>{element.email}</td>
                         <td>
                            <Button color='warning'>Update</Button>
                            <Button color='primary'>Detail</Button>
                            <Button color='danger'>Delete</Button>
                         </td>
                      </tr>
                  ))}
                </tbody>
             </Table>
          </Container>     
        </div>
      )
     }
}

