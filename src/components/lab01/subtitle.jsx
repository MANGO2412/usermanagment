import React from "react";


export default class Subtitle extends React.Component{
    render(){
        const {text,colorV}=this.props;
        return(
            <h1 style={{color:colorV}}>{text}</h1>
        );
    }
}